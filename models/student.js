const Student = function (
  id,
  fullName,
  type,
  math,
  physics,
  chemistry,
  trainingPoint
) {
  this.id = id;
  this.fullName = fullName;
  this.type = type;
  this.math = math;
  this.physics = physics;
  this.chemistry = chemistry;
  this.trainingPoint = trainingPoint;

  this.calcAverage = function () {
    return Math.floor((this.physics + this.math + this.chemistry) / 3);
  };
};
