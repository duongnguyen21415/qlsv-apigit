/**
 * Ứng dụng quản lý sinh viên
 * Công việc:
 *   1.Tạo lớp đối tượng sinh viên
 *   2.Xây giao diện UI
 *   3.CRUD Sinh viên
 *   4.Lưu trữ sinh viên
 *   5.Tìm kiếm sinh viên (mã vs tên)
 *   6.Validation: kiểm tra dữ liệu
 */

const handleCreateStudent = function () {
  //1.dom input lấy value
  const id = document.getElementById("txtMaSV").value;
  const fullName = document.getElementById("txtTenSV").value;
  const type = document.getElementById("loaiSV").value;
  const math = +document.getElementById("txtDiemToan").value;
  const physics = +document.getElementById("txtDiemLy").value;
  const chemistry = +document.getElementById("txtDiemHoa").value;
  const trainingPoint = +document.getElementById("txtDiemRenLuyen").value;

  //tạo ra một object student lưu info
  const newStudent = new Student(
    id,
    fullName,
    type,
    math,
    physics,
    chemistry,
    trainingPoint
  );
  //call api lưu student vào database
  var promise = axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/students",
    method: "POST",
    data: newStudent,
  });
  promise
    .then(function (res) {
      console.log(res);
      //gọi lại api fetchStudents
      fetchStudents();
    })
    .catch(function (error) {
      console.log(error);
    });
};

//yêu cầu: data phải là 1 array chưa đối tượng student
const createTable = function (data) {
  var studentHTML = "";
  for (var i = 0; i < data.length; i++) {
    studentHTML += `<tr>
        <td> ${data[i].id} </td> 
        <td>${data[i].fullName}</td> 
        <td>${data[i].type}</td> 
        <td>${data[i].calcAverage()}</td> 
        <td>${data[i].trainingPoint}</td>
        <td>
            <button onclick="handleDeleteStudent('${
              data[i].id
            }')" style="width:40px; height:40px" class="btn btn-danger rounded-circle">
                <i class="fa fa-trash"></i>
            </button>
            <button onclick="handleGetUpdatedStudent('${
              data[i].id
            }')" style="width:40px; height:40px" class="btn btn-info rounded-circle">
                <i class="fa fa-pencil-alt"></i>
            </button>
        </td>
      </tr>`;
  }
  document.getElementById("tbodySinhVien").innerHTML = studentHTML;
};

//input: id sinh viên
const handleDeleteStudent = function (id) {
  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/students/" + id,
    method: "DELETE",
  })
    .then(function (res) {
      console.log(res);
      fetchStudents();
    })
    .catch(function (error) {
      console.log(error);
    });
};

const handleGetUpdatedStudent = function (id) {
  // call api gửi request cho backend, yêu cầu lấy chi tiết students theo idName
  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/students/" + id,
    method: "GET",
  })
    .then(function (res) {
      console.log(res);
      //đưa dữ liệu lên form
      document.getElementById("txtMaSV").value = res.data.id;
      document.getElementById("txtTenSV").value = res.data.fullName;
      document.getElementById("loaiSV").value = res.data.type;
      document.getElementById("txtDiemToan").value = res.data.math;
      document.getElementById("txtDiemLy").value = res.data.physics;
      document.getElementById("txtDiemHoa").value = res.data.chemistry;
      document.getElementById("txtDiemRenLuyen").value = res.data.trainingPoint;
    })
    .catch(function (error) {
      console.log(error);
    });

  document.getElementById("txtMaSV").setAttribute("disabled", true);
};

// Phần 2: luu dữ liệu sinh viên sửa vào hệ thống
const handleUpdateStudent = function () {
  //lấy dữ liệu người dùng mới sửa
  const id = document.getElementById("txtMaSV").value;
  const fullName = document.getElementById("txtTenSV").value;
  const type = document.getElementById("loaiSV").value;
  const math = +document.getElementById("txtDiemToan").value;
  const physics = +document.getElementById("txtDiemLy").value;
  const chemistry = +document.getElementById("txtDiemHoa").value;
  const trainingPoint = +document.getElementById("txtDiemRenLuyen").value;

  const updatedStudent = new Student(
    id,
    fullName,
    type,
    math,
    physics,
    chemistry,
    trainingPoint
  );

  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/students/" + id,
    method: "PUT",
    data: updatedStudent,
  })
    .then(function (res) {
      console.log(res);
      fetchStudents();
    })
    .catch(function (error) {
      console.log(error);
    });
};

//FETCH STUDENTS FROM DB
const fetchStudents = function () {
  //promise: - pending
  //         - resolve (fulfill)
  //         - reject
  var promise = axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/students",
    method: "GET",
  });
  // thành công=> then()  fail => catch()
  promise
    .then(function (res) {
      //code chạy khi data trả về thành công
      console.log(res);
      //chuyển đổi data:
      var mappedData = mapData(res.data);
      createTable(mappedData);
    })
    .catch(function (error) {
      console.log(error);
    });
};
fetchStudents();

//chuyển đổi data từ data của backend => data của mình
//input: data của backend
//output: data của mình

const mapData = function (dataFromDB) {
  var mappedData = [];
  for (var i = 0; i < dataFromDB.length; i++) {
    const mappedStudent = new Student(
      dataFromDB[i].id,
      dataFromDB[i].fullName,
      dataFromDB[i].type,
      dataFromDB[i].math,
      dataFromDB[i].physics,
      dataFromDB[i].chemistry,
      dataFromDB[i].trainingPoint
    );
    mappedData.push(mappedStudent);
  }
  return mappedData;
};
